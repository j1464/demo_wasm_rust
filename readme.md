# Demo wasm in Rust

1. Установка Rust 
2. Установка wasm-pack

``` bat
@rem Для Windows и если у кого не собирается openssl какой то там...
cargo install wasm-pack --no-default-features

@rem В остальных случаях достаточно вот так 
cargo install wasm-pack
```

3. Создать библиотечный крейт

``` bat
cargo new --lib libname
```

4. Убеждаемся что Cargo.toml выглядит так

``` ini
[package]
name = "demo_wasm"
version = "0.1.0"
edition = "2021"

[lib]
# Обязательно
crate-type = ["cdylib"]


[dependencies]
# Обязательно с поправкой на актуальную версию
wasm-bindgen = "0.2.83"
```

``` rust
// lib.rs
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
extern {
    pub fn alert(s: &str);
}

#[wasm_bindgen]
pub fn hello_wasm() {
    alert(&format!("Hello from Rust code"));
}
```

5. Собираем это все 

```
wasm-pack build --target web
```

6. Создаем http сервер в той папке в которой будет корень сервера.

На примере ноды

```
npm install http-server -g
```

Запустить сервер 
```
http-server --cors -d
```

7. Закидываем в рут папку pkg и index.html в этом репозитории 

8. Готово.